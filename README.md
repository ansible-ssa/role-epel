# README

This role provides a distribution indepedent mechanism to setup and enable the EPEL repository.

Currently it supports the following distributions:

- RHEL 7

- RHEL 8

- CentOS 7
